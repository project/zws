<?php

/**
 * @file
 * Admin settings for the Zanox Web Services API module.
 */

/**
 * Admin settings form for the Zanox Web Services API module
 */
function zws_admin_settings() {
  $form = array();
  $form['zws'] = array(
    '#type' => 'fieldset',
    '#title' => t('Zanox Web Services API settings')
  );

  $form['zws']['zws_application_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#default_value' => variable_get('zws_application_id', ''),
    '#required' => TRUE,
    '#size' => 30,
    '#maxlength' => 128,
    '#description' => t('Enter your application ID to access the Zanox Web Services API.')
  );

  $form['zws']['zws_shared_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Shared Key'),
    '#default_value' => variable_get('zws_shared_key', ''),
    '#required' => TRUE,
    '#size' => 30,
    '#maxlength' => 128,
    '#description' => t('Enter your Shared Key to access the Zanox Web Services API.')
  );

  return system_settings_form($form);
}